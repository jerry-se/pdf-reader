#include "stdafx.h"
#include "CPdfView.h"
#include "DocManager.h"

BOOL CPdfView::PreTranslateMessage(MSG* pMsg)
{
  pMsg;
  return FALSE;
}

LRESULT CPdfView::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
  return 0;
}

LRESULT CPdfView::OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
  SetMsgHandled(FALSE);
  return 0;
}

LRESULT CPdfView::OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
  CPaintDC dc(m_hWnd);

  //TODO: Add your drawing code here
  RECT rc;
  GetClientRect(&rc);
  dc.SaveDC();
  //dc.SetTextColor(RGB(0, 0, 0));
  //dc.ExtTextOut(0, 0, ETO_OPAQUE, &rc, L"WTL Hello", 9, NULL);
  DOCUMENTMANAGER->DrawDocument(dc, rc);
  dc.RestoreDC(-1);

  return 0;
}
