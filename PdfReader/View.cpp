#include "stdafx.h"
#include "View.h"
#include "DocManager.h"
#include "ShellMgr.h"
#include <shlobj.h>

BOOL CView::PreTranslateMessage(MSG* pMsg)
{
  pMsg;
  return FALSE;
}

LRESULT CView::OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
  //m_tabCtrl.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);
  m_tabCtrl.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | TCS_TABS | TCS_OWNERDRAWFIXED);
  m_wndSplitter.Create(m_hWnd, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

  m_wndFolderTree.Create(m_wndSplitter, _T("Folders"), WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

  m_wndTreeView.Create(m_wndFolderTree, rcDefault, NULL,
    WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN |
    TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS,
    WS_EX_CLIENTEDGE);

  m_wndFolderTree.SetClient(m_wndTreeView);

  m_wndPdfView.Create(m_wndSplitter, rcDefault, NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN);

  InitViews();

  //UpdateLayout();

  m_wndSplitter.SetSplitterPanes(m_wndFolderTree, m_wndPdfView);

  RECT rect;
  GetClientRect(&rect);
  m_wndSplitter.SetSplitterPos((rect.right - rect.left) / 4);
  return 0;
}

LRESULT CView::OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled)
{
  RECT rc;
  GetClientRect(&rc);
  m_tabCtrl.SetWindowPos(NULL, rc.left, rc.top, rc.right - rc.left, 30, SWP_NOZORDER | SWP_NOACTIVATE);
  m_wndSplitter.SetWindowPos(NULL, rc.left, rc.top + 30, rc.right - rc.left, rc.bottom - rc.top - 30, SWP_NOZORDER | SWP_NOACTIVATE);
  return 0;
}

LRESULT CView::OnDrawItem(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
  DRAWITEMSTRUCT *dis = reinterpret_cast<DRAWITEMSTRUCT *>(lParam);
  if (dis->CtlType == ODT_TAB)
    return ::SendMessage(dis->hwndItem, WM_DRAWITEM, wParam, lParam);
  return FALSE;
}

LRESULT CView::OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
{
  CPaintDC dc(m_hWnd);

  //TODO: Add your drawing code here
  //RECT rc;
  //GetClientRect(&rc);
  //dc.SaveDC();
  ////dc.SetTextColor(RGB(0, 0, 0));
  ////dc.ExtTextOut(0, 0, ETO_OPAQUE, &rc, L"WTL Hello", 9, NULL);
  //if (g_doc && g_doc->document()) {
  //  int first_page = g_doc->GetFirstPageNum();
  //  FPDF_PAGE pdfpage = g_doc->LoadPage(first_page);
  //  FPDF_RenderPage(dc, pdfpage, rc.left, rc.top, rc.right - rc.left, rc.bottom - rc.top, 0, 0);
  //}
  //dc.RestoreDC(-1);

  return 0;
}

void CView::InitViews()
{
  // Get Desktop folder
  CShellItemIDList spidl;
  HRESULT hRet = ::SHGetSpecialFolderLocation(m_hWnd, CSIDL_DESKTOP, &spidl);
  hRet;	// avoid level 4 warning
  ATLASSERT(SUCCEEDED(hRet));

  // Get system image lists
  SHFILEINFO sfi = { 0 };
  memset(&sfi, 0, sizeof(SHFILEINFO));
  HIMAGELIST hImageListSmall = (HIMAGELIST)::SHGetFileInfo(spidl, 0, &sfi, sizeof(sfi), SHGFI_PIDL | SHGFI_SYSICONINDEX | SHGFI_SMALLICON);
  ATLASSERT(hImageListSmall != NULL);

  // Set tree view image list
  m_wndTreeView.SetImageList(hImageListSmall, 0);
}

void CView::LoadDocument(LPCTSTR lpszFile)
{
  if (DOCUMENTMANAGER->LoadDocument(lpszFile)) {
    m_tabCtrl.InsertItem(0, L"brewpub.pdf");
    m_wndPdfView.Invalidate();
  }
}
