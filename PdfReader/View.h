// View.h : interface of the CView class
//
/////////////////////////////////////////////////////////////////////////////

#pragma once
#include <atlsplit.h>
#include <atlctrls.h>
#include <atlctrlx.h>
#include "CDocTabCtrl.h"
#include "CPdfView.h"

class CMyPaneContainer : public CPaneContainerImpl<CMyPaneContainer>
{
public:
  DECLARE_WND_CLASS_EX(_T("WtlExplorer_PaneContainer"), 0, -1)

  void DrawPaneTitle(CDCHandle dc)
  {
    RECT rect = { 0 };
    GetClientRect(&rect);

    if (IsVertical())
    {
      rect.right = rect.left + m_cxyHeader;
      dc.DrawEdge(&rect, EDGE_ETCHED, BF_LEFT | BF_TOP | BF_BOTTOM | BF_ADJUST);
      dc.FillRect(&rect, COLOR_3DFACE);
    }
    else
    {
      rect.bottom = rect.top + m_cxyHeader;
      // we don't want this edge
      //			dc.DrawEdge(&rect, EDGE_ETCHED, BF_LEFT | BF_TOP | BF_RIGHT | BF_ADJUST);
      dc.FillRect(&rect, COLOR_3DFACE);
      // draw title only for horizontal pane container
      dc.SetTextColor(::GetSysColor(COLOR_WINDOWTEXT));
      dc.SetBkMode(TRANSPARENT);
      HFONT hFontOld = dc.SelectFont(GetTitleFont());
      rect.left += m_cxyTextOffset;
      rect.right -= m_cxyTextOffset;
      if (m_tb.m_hWnd != NULL)
        rect.right -= m_cxToolBar;;
      dc.DrawText(m_szTitle, -1, &rect, DT_LEFT | DT_SINGLELINE | DT_VCENTER | DT_END_ELLIPSIS);
      dc.SelectFont(hFontOld);
    }
  }
};

class CView : public CWindowImpl<CView>
{
public:
  DECLARE_WND_CLASS(NULL)

  CDocTabCtrl m_tabCtrl;
  CSplitterWindow m_wndSplitter;
  //CPaneContainer m_wndFolderTree;
  CMyPaneContainer m_wndFolderTree;
  CTreeViewCtrlEx m_wndTreeView;
  CPdfView m_wndPdfView;

  BOOL PreTranslateMessage(MSG* pMsg);

  BEGIN_MSG_MAP(CView)
    MESSAGE_HANDLER(WM_CREATE, OnCreate)
    MESSAGE_HANDLER(WM_SIZE, OnSize)
    MESSAGE_HANDLER(WM_DRAWITEM, OnDrawItem)
    MESSAGE_HANDLER(WM_PAINT, OnPaint)
  END_MSG_MAP()

  // Handler prototypes (uncomment arguments if needed):
  //	LRESULT MessageHandler(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/)
  //	LRESULT CommandHandler(WORD /*wNotifyCode*/, WORD /*wID*/, HWND /*hWndCtl*/, BOOL& /*bHandled*/)
  //	LRESULT NotifyHandler(int /*idCtrl*/, LPNMHDR /*pnmh*/, BOOL& /*bHandled*/)

  LRESULT OnCreate(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);
  LRESULT OnSize(UINT /*uMsg*/, WPARAM wParam, LPARAM /*lParam*/, BOOL& bHandled);
  LRESULT OnDrawItem(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM lParam, BOOL& bHandled);
  LRESULT OnPaint(UINT /*uMsg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL& /*bHandled*/);

  void InitViews();
  void LoadDocument(LPCTSTR lpszFile);

};
