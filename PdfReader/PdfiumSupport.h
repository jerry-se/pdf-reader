// Copyright 2015 PDFium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef TESTING_TEST_SUPPORT_H_
#define TESTING_TEST_SUPPORT_H_

#include <stdlib.h>

#include <memory>
#include <string>
#include <vector>
#include <fstream>

#include "fpdfview.h"
#include "fpdf_save.h"

namespace pdfium {
  // Used with std::unique_ptr to free() objects that can't be deleted.
  struct FreeDeleter {
    inline void operator()(void* ptr) const { free(ptr); }
  };
}  // namespace pdfium

// Reads the entire contents of a file into a newly alloc'd buffer.
std::unique_ptr<char, pdfium::FreeDeleter> GetFileContents(const char* filename,
  size_t* retlen);

std::vector<std::string> StringSplit(const std::string& str, char delimiter);

// Converts a FPDF_WIDESTRING to a std::string.
// Deals with differences between UTF16LE and UTF8.
//std::string GetPlatformString(FPDF_WIDESTRING wstr);

// Converts a FPDF_WIDESTRING to a std::wstring.
// Deals with differences between UTF16LE and wchar_t.
std::wstring GetPlatformWString(FPDF_WIDESTRING wstr);

// Returns a newly allocated FPDF_WIDESTRING.
// Deals with differences between UTF16LE and wchar_t.
std::unique_ptr<unsigned short, pdfium::FreeDeleter> GetFPDFWideString(
  const std::wstring& wstr);

std::string CryptToBase16(const uint8_t* digest);
//std::string GenerateMD5Base16(const uint8_t* data, uint32_t size);

class TestLoader {
public:
  TestLoader(const char* pBuf, size_t len);
  static int GetBlock(void* param,
    unsigned long pos,
    unsigned char* pBuf,
    unsigned long size);

private:
  const char* const m_pBuf;
  const size_t m_Len;
};

class PdfFileWrite : public FPDF_FILEWRITE
{
public:
  PdfFileWrite();
  ~PdfFileWrite();

  bool OpenSaveDocument(LPCWSTR lpszPath);
protected:
  static int WriteBlockCallback(struct FPDF_FILEWRITE_* pFileWrite, const void* data, unsigned long size);

private:
  std::string		data_string_;
  std::ofstream	filestream_;
};

#endif  // TESTING_TEST_SUPPORT_H_
