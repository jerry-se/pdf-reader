#include "stdafx.h"
#include "PdfDocument.h"
#include "fpdf_edit.h"

FPDF_BOOL Is_Data_Avail(FX_FILEAVAIL* pThis, size_t offset, size_t size) {
  return true;
}

void Add_Segment(FX_DOWNLOADHINTS* pThis, size_t offset, size_t size) {}

CPdfDocument::CPdfDocument()
  : default_delegate_(new Delegate()),
  document_(nullptr),
  form_handle_(nullptr),
  avail_(nullptr),
  external_isolate_(nullptr),
  loader_(nullptr),
  file_length_(0),
  file_contents_(nullptr)
{
  memset(&hints_, 0, sizeof(hints_));
  memset(&file_access_, 0, sizeof(file_access_));
  memset(&file_avail_, 0, sizeof(file_avail_));
  delegate_ = default_delegate_.get();

#ifdef PDF_ENABLE_V8
#ifdef V8_USE_EXTERNAL_STARTUP_DATA
  if (g_v8_natives && g_v8_snapshot) {
    InitializeV8ForPDFium(g_exe_path, std::string(), nullptr, nullptr,
      &platform_);
  }
  else {
    g_v8_natives = new v8::StartupData;
    g_v8_snapshot = new v8::StartupData;
    InitializeV8ForPDFium(g_exe_path, std::string(), g_v8_natives,
      g_v8_snapshot, &platform_);
  }
#else
  InitializeV8ForPDFium(g_exe_path, &platform_);
#endif  // V8_USE_EXTERNAL_STARTUP_DATA
#endif  // FPDF_ENABLE_V8
}

CPdfDocument::~CPdfDocument()
{
#ifdef PDF_ENABLE_V8
  v8::V8::ShutdownPlatform();
  delete platform_;
#endif  // PDF_ENABLE_V8
}

void CPdfDocument::SetUp()
{
  UNSUPPORT_INFO* info = static_cast<UNSUPPORT_INFO*>(this);
  memset(info, 0, sizeof(UNSUPPORT_INFO));
  info->version = 1;
  info->FSDK_UnSupport_Handler = UnsupportedHandlerTrampoline;
  FSDK_SetUnSpObjProcessHandler(info);
}

void CPdfDocument::TearDown()
{
  if (document_) {
    FORM_DoDocumentAAction(form_handle_, FPDFDOC_AACTION_WC);
    FPDFDOC_ExitFormFillEnvironment(form_handle_);
    FPDF_CloseDocument(document_);
  }

  FPDFAvail_Destroy(avail_);
  FPDF_DestroyLibrary();
  delete loader_;
}

bool CPdfDocument::CreateEmptyDocument()
{
  document_ = FPDF_CreateNewDocument();
  if (!document_)
    return false;

  form_handle_ = SetupFormFillEnvironment(document_);
  return true;
}

bool CPdfDocument::OpenDocument(const std::string& filename, const char* password /* = nullptr */, bool must_linearize /* = false */)
{
  std::string file_path = filename;
  //if (!PathService::GetTestFilePath(filename, &file_path))
  //  return false;
  file_contents_ = GetFileContents(file_path.c_str(), &file_length_);
  if (!file_contents_)
    return false;

  //EXPECT_TRUE(!loader_);
  loader_ = new TestLoader(file_contents_.get(), file_length_);
  file_access_.m_FileLen = static_cast<unsigned long>(file_length_);
  file_access_.m_GetBlock = TestLoader::GetBlock;
  file_access_.m_Param = loader_;
  return OpenDocumentHelper(password, must_linearize, &file_avail_, &hints_,
    &file_access_, &document_, &avail_, &form_handle_);
}

void CPdfDocument::PrintLastError()
{
  unsigned long err = FPDF_GetLastError();
  fprintf(stderr, "Load pdf docs unsuccessful: ");
  switch (err) {
  case FPDF_ERR_SUCCESS:
    fprintf(stderr, "Success");
    break;
  case FPDF_ERR_UNKNOWN:
    fprintf(stderr, "Unknown error");
    break;
  case FPDF_ERR_FILE:
    fprintf(stderr, "File not found or could not be opened");
    break;
  case FPDF_ERR_FORMAT:
    fprintf(stderr, "File not in PDF format or corrupted");
    break;
  case FPDF_ERR_PASSWORD:
    fprintf(stderr, "Password required or incorrect password");
    break;
  case FPDF_ERR_SECURITY:
    fprintf(stderr, "Unsupported security scheme");
    break;
  case FPDF_ERR_PAGE:
    fprintf(stderr, "Page not found or content error");
    break;
  default:
    fprintf(stderr, "Unknown error %ld", err);
  }
  fprintf(stderr, ".\n");
  return;
}

bool CPdfDocument::OpenDocumentHelper(const char* password, bool must_linearize, FX_FILEAVAIL* file_avail, FX_DOWNLOADHINTS* hints, FPDF_FILEACCESS* file_access, FPDF_DOCUMENT* document, FPDF_AVAIL* avail, FPDF_FORMHANDLE* form_handle)
{
  file_avail->version = 1;
  file_avail->IsDataAvail = Is_Data_Avail;

  hints->version = 1;
  hints->AddSegment = Add_Segment;

  *avail = FPDFAvail_Create(file_avail, file_access);

  if (FPDFAvail_IsLinearized(*avail) == PDF_LINEARIZED) {
    *document = FPDFAvail_GetDocument(*avail, password);
    if (!*document)
      return false;

    int32_t nRet = PDF_DATA_NOTAVAIL;
    while (nRet == PDF_DATA_NOTAVAIL)
      nRet = FPDFAvail_IsDocAvail(*avail, hints);
    if (nRet == PDF_DATA_ERROR)
      return false;

    nRet = FPDFAvail_IsFormAvail(*avail, hints);
    if (nRet == PDF_FORM_ERROR || nRet == PDF_FORM_NOTAVAIL)
      return false;

    int page_count = FPDF_GetPageCount(*document);
    for (int i = 0; i < page_count; ++i) {
      nRet = PDF_DATA_NOTAVAIL;
      while (nRet == PDF_DATA_NOTAVAIL)
        nRet = FPDFAvail_IsPageAvail(*avail, i, hints);

      if (nRet == PDF_DATA_ERROR)
        return false;
    }
  }
  else {
    if (must_linearize)
      return false;

    *document = FPDF_LoadCustomDocument(file_access, password);
    if (!*document)
      return false;
  }
  *form_handle = SetupFormFillEnvironment(*document);
#ifdef PDF_ENABLE_XFA
  int docType = DOCTYPE_PDF;
  if (FPDF_HasXFAField(*document, &docType)) {
    if (docType != DOCTYPE_PDF)
      (void)FPDF_LoadXFA(*document);
  }
#endif  // PDF_ENABLE_XFA
  (void)FPDF_GetDocPermissions(*document);
  return true;
}

FPDF_FORMHANDLE CPdfDocument::SetupFormFillEnvironment(FPDF_DOCUMENT doc)
{
  IPDF_JSPLATFORM* platform = static_cast<IPDF_JSPLATFORM*>(this);
  memset(platform, '\0', sizeof(IPDF_JSPLATFORM));
  platform->version = 2;
  platform->app_alert = AlertTrampoline;
  platform->m_isolate = external_isolate_;

  FPDF_FORMFILLINFO* formfillinfo = static_cast<FPDF_FORMFILLINFO*>(this);
  memset(formfillinfo, 0, sizeof(FPDF_FORMFILLINFO));
#ifdef PDF_ENABLE_XFA
  formfillinfo->version = 2;
#else   // PDF_ENABLE_XFA
  formfillinfo->version = 1;
#endif  // PDF_ENABLE_XFA
  formfillinfo->FFI_SetTimer = SetTimerTrampoline;
  formfillinfo->FFI_KillTimer = KillTimerTrampoline;
  formfillinfo->FFI_GetPage = GetPageTrampoline;
  formfillinfo->m_pJsPlatform = platform;
  FPDF_FORMHANDLE form_handle =
    FPDFDOC_InitFormFillEnvironment(doc, formfillinfo);
  FPDF_SetFormFieldHighlightColor(form_handle, 0, 0xFFE4DD);
  FPDF_SetFormFieldHighlightAlpha(form_handle, 100);
  return form_handle;
}

FPDF_PAGE CPdfDocument::Delegate::GetPage(FPDF_FORMFILLINFO* info,
  FPDF_DOCUMENT document,
  int page_index) {
  CPdfDocument* test = static_cast<CPdfDocument*>(info);
  auto it = test->page_map_.find(page_index);
  return it != test->page_map_.end() ? it->second : nullptr;
}

void CPdfDocument::DoOpenActions()
{
  //ASSERT(form_handle_);
  FORM_DoDocumentJSAction(form_handle_);
  FORM_DoDocumentOpenAction(form_handle_);
}

int CPdfDocument::GetFirstPageNum()
{
  int first_page = FPDFAvail_GetFirstPageNum(document_);
  (void)FPDFAvail_IsPageAvail(avail_, first_page, &hints_);
  return first_page;
}

int CPdfDocument::GetPageCount()
{
  int page_count = FPDF_GetPageCount(document_);
  for (int i = 0; i < page_count; ++i)
    (void)FPDFAvail_IsPageAvail(avail_, i, &hints_);
  return page_count;
}

FPDF_PAGE CPdfDocument::LoadPage(int page_number)
{
  //ASSERT(form_handle_);
  // First check whether it is loaded already.
  auto it = page_map_.find(page_number);
  if (it != page_map_.end())
    return it->second;

  FPDF_PAGE page = FPDF_LoadPage(document_, page_number);
  if (!page)
    return nullptr;

  FORM_OnAfterLoadPage(page, form_handle_);
  FORM_DoPageAAction(page, form_handle_, FPDFPAGE_AACTION_OPEN);
  // Cache the page.
  page_map_[page_number] = page;
  //page_reverse_map_[page] = page_number;
  return page;
}

FPDF_BITMAP CPdfDocument::RenderPage(FPDF_PAGE page)
{
  return RenderPageWithFlags(page, form_handle_, 0);
}

FPDF_BITMAP CPdfDocument::RenderPageWithFlags(FPDF_PAGE page, FPDF_FORMHANDLE handle, int flags)
{
  int width = static_cast<int>(FPDF_GetPageWidth(page));
  int height = static_cast<int>(FPDF_GetPageHeight(page));
  int alpha = FPDFPage_HasTransparency(page) ? 1 : 0;
  FPDF_BITMAP bitmap = FPDFBitmap_Create(width, height, alpha);
  FPDF_DWORD fill_color = alpha ? 0x00000000 : 0xFFFFFFFF;
  FPDFBitmap_FillRect(bitmap, 0, 0, width, height, fill_color);
  FPDF_RenderPageBitmap(bitmap, page, 0, 0, width, height, 0, flags);
  FPDF_FFLDraw(handle, bitmap, page, 0, 0, width, height, 0, flags);
  return bitmap;
}

void CPdfDocument::UnloadPage(FPDF_PAGE page)
{
  //ASSERT(form_handle_);
  FORM_DoPageAAction(page, form_handle_, FPDFPAGE_AACTION_CLOSE);
  FORM_OnBeforeClosePage(page, form_handle_);
  FPDF_ClosePage(page);

  //auto it = page_reverse_map_.find(page);
  //if (it == page_reverse_map_.end())
  //  return;

  //page_map_.erase(it->second);
  //page_reverse_map_.erase(it);
}

// static
void CPdfDocument::UnsupportedHandlerTrampoline(UNSUPPORT_INFO* info, int type)
{
  CPdfDocument* test = static_cast<CPdfDocument*>(info);
  test->delegate_->UnsupportedHandler(type);
}

//static
int CPdfDocument::AlertTrampoline(IPDF_JSPLATFORM* platform, FPDF_WIDESTRING message, FPDF_WIDESTRING title, int type, int icon)
{
  CPdfDocument* test = static_cast<CPdfDocument*>(platform);
  return test->delegate_->Alert(message, title, type, icon);
}

// static
int CPdfDocument::SetTimerTrampoline(FPDF_FORMFILLINFO* info, int msecs, TimerCallback fn)
{
  CPdfDocument* test = static_cast<CPdfDocument*>(info);
  return test->delegate_->SetTimer(msecs, fn);
}

// static
void CPdfDocument::KillTimerTrampoline(FPDF_FORMFILLINFO* info, int id)
{
  CPdfDocument* test = static_cast<CPdfDocument*>(info);
  return test->delegate_->KillTimer(id);
}

// static
FPDF_PAGE CPdfDocument::GetPageTrampoline(FPDF_FORMFILLINFO* info, FPDF_DOCUMENT document, int page_index)
{
  return static_cast<CPdfDocument*>(info)->delegate_->GetPage(info, document, page_index);
}
