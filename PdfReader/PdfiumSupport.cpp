#include "stdafx.h"
#include "PdfiumSupport.h"

std::unique_ptr<char, pdfium::FreeDeleter> GetFileContents(const char* filename,
  size_t* retlen) {
  FILE* file = fopen(filename, "rb");
  if (!file) {
    fprintf(stderr, "Failed to open: %s\n", filename);
    return nullptr;
  }
  (void)fseek(file, 0, SEEK_END);
  size_t file_length = ftell(file);
  if (!file_length) {
    return nullptr;
  }
  (void)fseek(file, 0, SEEK_SET);
  std::unique_ptr<char, pdfium::FreeDeleter> buffer(
    static_cast<char*>(malloc(file_length)));
  if (!buffer) {
    return nullptr;
  }
  size_t bytes_read = fread(buffer.get(), 1, file_length, file);
  (void)fclose(file);
  if (bytes_read != file_length) {
    fprintf(stderr, "Failed to read: %s\n", filename);
    return nullptr;
  }
  *retlen = bytes_read;
  return buffer;
}

//std::string GetPlatformString(FPDF_WIDESTRING wstr) {
//  CFX_WideString wide_string =
//    CFX_WideString::FromUTF16LE(wstr, CFX_WideString::WStringLength(wstr));
//  return std::string(wide_string.UTF8Encode().c_str());
//}

std::wstring GetPlatformWString(FPDF_WIDESTRING wstr) {
  if (!wstr)
    return nullptr;

  size_t characters = 0;
  while (wstr[characters])
    ++characters;

  std::wstring platform_string(characters, L'\0');
  for (size_t i = 0; i < characters + 1; ++i) {
    const unsigned char* ptr = reinterpret_cast<const unsigned char*>(&wstr[i]);
    platform_string[i] = ptr[0] + 256 * ptr[1];
  }
  return platform_string;
}

std::vector<std::string> StringSplit(const std::string& str, char delimiter) {
  std::vector<std::string> result;
  size_t pos = 0;
  while (1) {
    size_t found = str.find(delimiter, pos);
    if (found == std::string::npos)
      break;

    result.push_back(str.substr(pos, found - pos));
    pos = found + 1;
  }
  result.push_back(str.substr(pos));
  return result;
}

std::unique_ptr<unsigned short, pdfium::FreeDeleter> GetFPDFWideString(
  const std::wstring& wstr) {
  size_t length = sizeof(uint16_t) * (wstr.length() + 1);
  std::unique_ptr<unsigned short, pdfium::FreeDeleter> result(
    static_cast<unsigned short*>(malloc(length)));
  char* ptr = reinterpret_cast<char*>(result.get());
  size_t i = 0;
  for (wchar_t w : wstr) {
    ptr[i++] = w & 0xff;
    ptr[i++] = (w >> 8) & 0xff;
  }
  ptr[i++] = 0;
  ptr[i] = 0;
  return result;
}

std::string CryptToBase16(const uint8_t* digest) {
  static char const zEncode[] = "0123456789abcdef";
  std::string ret;
  ret.resize(32);
  for (int i = 0, j = 0; i < 16; i++, j += 2) {
    uint8_t a = digest[i];
    ret[j] = zEncode[(a >> 4) & 0xf];
    ret[j + 1] = zEncode[a & 0xf];
  }
  return ret;
}

//std::string GenerateMD5Base16(const uint8_t* data, uint32_t size) {
//  uint8_t digest[16];
//  CRYPT_MD5Generate(data, size, digest);
//  return CryptToBase16(digest);
//}

TestLoader::TestLoader(const char* pBuf, size_t len)
  : m_pBuf(pBuf), m_Len(len) {
}

// static
int TestLoader::GetBlock(void* param,
  unsigned long pos,
  unsigned char* pBuf,
  unsigned long size) {
  TestLoader* pLoader = static_cast<TestLoader*>(param);
  if (pos + size < pos || pos + size > pLoader->m_Len)
    return 0;

  memcpy(pBuf, pLoader->m_pBuf + pos, size);
  return 1;
}

//////////////////////////////////////////////////////////////////////////
PdfFileWrite::PdfFileWrite()
{
  FPDF_FILEWRITE::version = 1;
  FPDF_FILEWRITE::WriteBlock = WriteBlockCallback;
}

PdfFileWrite::~PdfFileWrite()
{
  if (filestream_.is_open())
    filestream_.close();
}

bool PdfFileWrite::OpenSaveDocument(LPCWSTR lpszPath)
{
  if (filestream_.is_open()) filestream_.close();
  filestream_.open(lpszPath, std::ios::binary);
  return filestream_.is_open();
}

// static
int PdfFileWrite::WriteBlockCallback(struct FPDF_FILEWRITE_* pFileWrite, const void* data, unsigned long size) {
  PdfFileWrite* pThis = static_cast<PdfFileWrite*>(pFileWrite);

  pThis->data_string_.append(static_cast<const char*>(data), size);

  if (pThis->filestream_.is_open())
    pThis->filestream_.write(static_cast<const char*>(data), size);
  else {
    printf("WriteBlock failed because the file is not opened.\n");
    return 0;
  }

  printf("WriteBlock %lu byte.\n", size);
  return 1;
}
