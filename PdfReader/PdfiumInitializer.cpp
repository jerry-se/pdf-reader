#include "stdafx.h"
#include "PdfiumInitializer.h"
#include <fpdfview.h>

PdfiumInitializer::PdfiumInitializer()
{
	InitializeSDK();
}

PdfiumInitializer::~PdfiumInitializer()
{
	ShutdownSDK();
}

bool PdfiumInitializer::InitializeSDK()
{
	FPDF_LIBRARY_CONFIG config;
	config.version = 2;
	config.m_pUserFontPaths = nullptr;
	config.m_pIsolate = nullptr;
	config.m_v8EmbedderSlot = 0;
	FPDF_InitLibraryWithConfig(&config);

	return true;
}

void PdfiumInitializer::ShutdownSDK()
{
	FPDF_DestroyLibrary();
}