#pragma once
#include <string>

namespace str {
  //FORCEINLINE
  std::string w2a(const wchar_t *s);
  //FORCEINLINE
  std::string w2a(std::wstring s);

  std::wstring a2w(const char *s);

  std::wstring a2w(std::string s);
}
