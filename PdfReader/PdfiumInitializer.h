#pragma once

class PdfiumInitializer
{
public:
	PdfiumInitializer();
	~PdfiumInitializer();

private:
	bool InitializeSDK();
	void ShutdownSDK();
};

