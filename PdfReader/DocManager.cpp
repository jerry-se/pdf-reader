#include "stdafx.h"
#include "DocManager.h"
#include "StrUtil.h"

DocumentManager::DocumentManager() : cur_docment_(nullptr)
{
}

DocumentManager::~DocumentManager()
{
}

DocumentManager* DocumentManager::Instance()
{
  static DocumentManager doc_manager;
  return &doc_manager;
}

void DocumentManager::Initialize()
{

}

void DocumentManager::UnInitialize()
{
  for (auto& iter : map_documents_) {
    iter.second->TearDown();
  }
  cur_docment_ = nullptr;
}

bool DocumentManager::LoadDocument(LPCTSTR lpszFile)
{
  if (map_documents_.find(lpszFile) != map_documents_.end())
    return false;
  std::shared_ptr<CPdfDocument> doc = std::make_shared<CPdfDocument>();
  doc->SetUp();
  if (doc->OpenDocument(str::w2a(lpszFile))) {
    map_documents_[lpszFile] = doc;
    cur_docment_ = doc.get();
    return true;
  }
  doc->PrintLastError();
  return false;
}

void DocumentManager::DrawDocument(HDC hdc, RECT rcArea)
{
  if (!cur_docment_ || !cur_docment_->document()) return;
  int first_page = cur_docment_->GetFirstPageNum();
  FPDF_PAGE pdfpage = cur_docment_->LoadPage(first_page);
  FPDF_RenderPage(hdc, pdfpage, rcArea.left, rcArea.top, rcArea.right - rcArea.left, rcArea.bottom - rcArea.top, 0, 0);
}
