#include "stdafx.h"
#include "CDocTabCtrl.h"
#include "AppConfig.h"

CDocTabCtrl::CDocTabCtrl()
{

}

CDocTabCtrl::~CDocTabCtrl()
{

}

HWND CDocTabCtrl::Create(HWND hWndParent, ATL::_U_RECT rect /* = NULL */, LPCTSTR szWindowName /* = NULL */, DWORD dwStyle /* = 0 */, DWORD dwExStyle /* = 0 */, ATL::_U_MENUorID MenuOrID /* = 0U */, LPVOID lpCreateParam /* = NULL */)
{
  HWND hwnd = CTabCtrl::Create(hWndParent, rect, szWindowName, dwStyle, dwExStyle, MenuOrID, lpCreateParam);
  ::SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
  m_tabBarDefaultProc = reinterpret_cast<WNDPROC>(::SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(TabBarPlus_Proc)));
  return hwnd;
}

LRESULT CDocTabCtrl::runProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
  switch (Message)
  {
  case WM_ERASEBKGND:
    return TRUE;
  case WM_DRAWITEM:
    drawItem((DRAWITEMSTRUCT *)lParam);
    return TRUE;
  default:
    break;
  }
  return ::CallWindowProc(m_tabBarDefaultProc, hwnd, Message, wParam, lParam);
}

LRESULT CALLBACK CDocTabCtrl::TabBarPlus_Proc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
  return (((CDocTabCtrl *)(::GetWindowLongPtr(hwnd, GWLP_USERDATA)))->runProc(hwnd, Message, wParam, lParam));
}

void CDocTabCtrl::drawItem(DRAWITEMSTRUCT *pDrawItemStruct)
{
  RECT rect = pDrawItemStruct->rcItem;

  int nTab = pDrawItemStruct->itemID;
  if (nTab < 0)
  {
    ::MessageBox(NULL, TEXT("nTab < 0"), TEXT(""), MB_OK);
  }

  bool isSelected = (nTab == GetCurSel());

  TCHAR label[MAX_PATH];
  TCITEM tci;
  tci.mask = TCIF_TEXT | TCIF_IMAGE;
  tci.pszText = label;
  tci.cchTextMax = MAX_PATH - 1;

  if (!GetItem(nTab, &tci))
  {
    ::MessageBox(NULL, TEXT("! TCM_GETITEM"), TEXT(""), MB_OK);
  }
  HDC hDC = pDrawItemStruct->hDC;

  int nSavedDC = ::SaveDC(hDC);

  ::SetBkMode(hDC, TRANSPARENT);
  HBRUSH hBrush = ::CreateSolidBrush(::GetSysColor(COLOR_WINDOW));//::GetSysColor(COLOR_BTNFACE)
  ::FillRect(hDC, &rect, hBrush);
  ::DeleteObject((HGDIOBJ)hBrush);

  static bool _isVertical = false;
  static bool _drawTopBar = true;

  // equalize drawing areas of active and inactive tabs
  int paddingDynamicTwoX = g_AppConfig._dpiManager.scaleX(2);
  int paddingDynamicTwoY = g_AppConfig._dpiManager.scaleY(2);
  if (isSelected)
  {
    // the drawing area of the active tab extends on all borders by default
    rect.top += ::GetSystemMetrics(SM_CYEDGE);
    rect.bottom -= ::GetSystemMetrics(SM_CYEDGE);
    rect.left += ::GetSystemMetrics(SM_CXEDGE);
    rect.right -= ::GetSystemMetrics(SM_CXEDGE);
    // the active tab is also slightly higher by default (use this to shift the tab cotent up bx two pixels if tobBar is not drawn)
    if (_isVertical)
    {
      rect.left += _drawTopBar ? paddingDynamicTwoX : 0;
      rect.right -= _drawTopBar ? 0 : paddingDynamicTwoX;
    }
    else
    {
      rect.top += _drawTopBar ? paddingDynamicTwoY : 0;
      rect.bottom -= _drawTopBar ? 0 : paddingDynamicTwoY;
    }
  }
  else
  {
    if (_isVertical)
    {
      rect.left += paddingDynamicTwoX;
      rect.right += paddingDynamicTwoX;
      rect.top -= paddingDynamicTwoY;
      rect.bottom += paddingDynamicTwoY;
    }
    else
    {
      rect.left -= paddingDynamicTwoX;
      rect.right += paddingDynamicTwoX;
      rect.top += paddingDynamicTwoY;
      rect.bottom += paddingDynamicTwoY;
    }
  }

  // the active tab's text with TCS_BUTTONS is lower than normal and gets clipped
  if (::GetWindowLongPtr(m_hWnd, GWL_STYLE) & TCS_BUTTONS)
  {
    if (_isVertical)
    {
      rect.left -= 2;
    }
    else
    {
      rect.top -= 2;
    }
  }

  // draw text
  //bool isStandardSize = (::SendMessage(_hParent, NPPM_INTERNAL_ISTABBARREDUCED, 0, 0) == TRUE);

  //if (isStandardSize)
  //{
  //  if (_isVertical)
  //    SelectObject(hDC, _hVerticalFont);
  //  else
  //    SelectObject(hDC, _hFont);
  //}
  //else
  //{
  //  if (_isVertical)
  //    SelectObject(hDC, _hVerticalLargeFont);
  //  else
  //    SelectObject(hDC, _hLargeFont);
  //}
  SIZE charPixel;
  ::GetTextExtentPoint(hDC, TEXT(" "), 1, &charPixel);
  int spaceUnit = charPixel.cx;

  TEXTMETRIC textMetrics;
  GetTextMetrics(hDC, &textMetrics);
  int textHeight = textMetrics.tmHeight;
  int textDescent = textMetrics.tmDescent;

  int Flags = DT_SINGLELINE | DT_NOPREFIX;

  // This code will read in one character at a time and remove every first ampersand (&).
  // ex. If input "test && test &&& test &&&&" then output will be "test & test && test &&&".
  // Tab's caption must be encoded like this because otherwise tab control would make tab too small or too big for the text.
  TCHAR decodedLabel[MAX_PATH];
  const TCHAR* in = label;
  TCHAR* out = decodedLabel;
  while (*in != 0)
    if (*in == '&')
      while (*(++in) == '&')
        *out++ = *in;
    else
      *out++ = *in++;
  *out = '\0';

  if (_isVertical)
  {
    // center text horizontally (rotated text is positioned as if it were unrotated, therefore manual positioning is necessary)
    Flags |= DT_LEFT;
    Flags |= DT_BOTTOM;
    rect.left += (rect.right - rect.left - textHeight) / 2;
    rect.bottom += textHeight;

    // ignoring the descent when centering (text elements below the base line) is more pleasing to the eye
    rect.left += textDescent / 2;
    rect.right += textDescent / 2;

    // 1 space distance to save icon
    rect.bottom -= spaceUnit;
  }
  else
  {
    // center text vertically
    Flags |= DT_LEFT;
    Flags |= DT_VCENTER;

    // ignoring the descent when centering (text elements below the base line) is more pleasing to the eye
    rect.top += textDescent / 2;
    rect.bottom += textDescent / 2;

    // 1 space distance to save icon
    rect.left += spaceUnit;
  }

  ::SetTextColor(hDC, RGB(0, 0, 0));
  //::ExtTextOut(hDC, rect.left, rect.top, ETO_OPAQUE, &rect, L"Hello", 5, NULL);
  ::DrawText(hDC, decodedLabel, lstrlen(decodedLabel), &rect, DT_CENTER);

  ::RestoreDC(hDC, -1);
}
