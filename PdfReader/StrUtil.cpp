#include "stdafx.h"
#include "StrUtil.h"

namespace str {

  std::string w2a(std::wstring s)
  {
    return w2a(s.c_str());
  }

  std::string w2a(const wchar_t *s)
  {
    std::string str;

    if (s != NULL)
    {
      int len = WideCharToMultiByte(CP_ACP, 0, s, -1, NULL, 0, NULL, NULL) - 1;

      if (len > 0)
      {//测试数据: s=神界 len=4
        str.reserve(len + 1);
        str.resize(len);
        len = WideCharToMultiByte(CP_ACP, 0, s, -1, (LPSTR)str.data(), len, NULL, NULL);
      }
    }

    return str.c_str();
  }


  std::wstring a2w(std::string s)
  {
    return a2w(s.c_str());
  }

  std::wstring a2w(const char *s)
  {
    std::wstring str;

    if (s != NULL)
    {
      int len = MultiByteToWideChar(CP_ACP, 0, s, -1, NULL, 0);

      if (len > 0)
      {//测试数据: s=神界 len=3
        str.reserve(len);
        str.resize(len - 1);
        MultiByteToWideChar(CP_ACP, 0, s, -1, (LPWSTR)str.data(), len);
      }
    }

    return str.c_str();
  }

}
