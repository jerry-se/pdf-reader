#pragma once
#include <string>
#include "dpiManager.h"

class CAppConfig
{
public:
  CAppConfig();
  ~CAppConfig();

  void InitConfig();

  const std::wstring& GetModulePath() const { return strModulePath; }
  const std::wstring& GetModuleFolder() const { return strModuleFolder; }
  const std::wstring& GetIniPath() const { return strIniPath; }
  const std::wstring& GetExamplePath() const { return strExamplePath; }

public:
  DPIManager _dpiManager;

private:
  //主程序的路径
  std::wstring strModulePath;
  //主程序所在的文件夹
  std::wstring strModuleFolder;
  //用户文档的配置路径
  std::wstring strIniPath;
  //示例文档所在的文件夹
  std::wstring strExamplePath;
};

extern CAppConfig g_AppConfig;
