#pragma once
#include <atlctrls.h>

class CDocTabCtrl : public CTabCtrl
{
public:
  CDocTabCtrl();
  ~CDocTabCtrl();

  HWND Create(HWND hWndParent, ATL::_U_RECT rect = NULL, LPCTSTR szWindowName = NULL,
    DWORD dwStyle = 0, DWORD dwExStyle = 0,
    ATL::_U_MENUorID MenuOrID = 0U, LPVOID lpCreateParam = NULL);

protected:
  WNDPROC m_tabBarDefaultProc = nullptr;

  LRESULT runProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);
  static LRESULT CALLBACK TabBarPlus_Proc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam);
  void drawItem(DRAWITEMSTRUCT *pDrawItemStruct);;

private:
};
