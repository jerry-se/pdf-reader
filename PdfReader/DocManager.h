#pragma once
#include "PdfDocument.h"

class DocumentManager
{
public:
  static DocumentManager* Instance();

  void Initialize();
  void UnInitialize();

  bool LoadDocument(LPCTSTR lpszFile);
  void DrawDocument(HDC hdc, RECT rcArea);

private:
  DocumentManager();
  ~DocumentManager();

  DocumentManager* doc_manager;
  CPdfDocument* cur_docment_;
  std::map<std::wstring, std::shared_ptr<CPdfDocument>> map_documents_;

};

#define DOCUMENTMANAGER DocumentManager::Instance()
